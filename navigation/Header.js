import * as React from 'react';
import {View, StyleSheet, Dimensions} from 'react-native';
import SearchInput from '../components/SearchInput';


const Header = () => {

  return (
    <View style={styles.searchContainer}>
      <SearchInput/>
    </View>
  );
};

const { width } = Dimensions.get('window');
const { height } = Dimensions.get('window');

const styles = StyleSheet.create({
  searchContainer: {
    flex: 1,
    minWidth: '100%',
    margin: 0,
    padding: 0,
    alignItems: 'stretch',
    justifyContent: 'center',
    marginTop: 0,
    width: width - 30,
  }
});

export default Header;

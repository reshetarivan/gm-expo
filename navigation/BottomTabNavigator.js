import * as React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Animated } from 'react-native';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import FavouritesScreen from '../screens/FavouritesScreen';
import { CONFIG } from '../config';
import PostAdScreen from '../screens/PostAdScreen';
import MessagesScreen from '../screens/MessagesScreen';
import ProfileScreen from '../screens/ProfileScreen';
import { selectShowNav } from '../redux/global/global.selector';

const BottomTab = createBottomTabNavigator();
const INITIAL_ROUTE_NAME = 'Home';

const mapStateToProps = createStructuredSelector({
  selectShowNav
});


export default connect(mapStateToProps, null)(({ selectShowNav, navigation, route }) =>

// Set the header title on the parent stack navigator depending on the
// currently active tab. Learn more in the documentation:
// https://reactnavigation.org/docs/en/screen-options-resolution.html

// navigation.setOptions({
//   // headerTitle: getHeaderTitle(route),
//   headerStyle: {
//     backgroundColor: Colors.panelBgColor,
//     opacity: 0.7
//   },
//   headerTitleStyle: {
//     color: "#000",
//   },
//   animationEnabled: true,
// });

// let mtLabel = Platform.OS === 'ios' || Platform.OS === 'android' ? 35 : 35;

// console.log('BOTTOM_TAB', selectShowTabMenu);


  (
    <BottomTab.Navigator
      initialRouteName={INITIAL_ROUTE_NAME}
      screenOptions={{ gestureEnabled: true }}
      tabBarOptions={{
        activeTintColor: CONFIG.COLORS.PRIMARY,
        inactiveTintColor: CONFIG.COLORS.SECONDARY,
        style: {
          display: selectShowNav ? 'flex' : 'none',
          backgroundColor: CONFIG.COLORS.WHITE,
          opacity: 1,
          height: selectShowNav ? CONFIG.FOOTER.HEIGHT : 0,
          elevation: CONFIG.BUTTON.ELEVATION,
          shadowOffset: {
            width: 0,
            height: 0
          },
          shadowRadius: CONFIG.BUTTON.SHADOW_BLUR,
          shadowColor: CONFIG.BUTTON.SHADOW_BOTTOM_COLOR,
          shadowOpacity: CONFIG.BUTTON.SHADOW_OPACITY,
        },
        allowFontScaling: false,
        tabStyle: {
          flexDirection: 'column',
          justifyContent: 'flex-start',
          alignItems: 'center',
          paddingTop: 0,
        },
        labelStyle: {
          marginTop: 40,
          marginLeft: 0,
          position: 'absolute',
        }
      }}
    >
      <BottomTab.Screen
        name="Home"
        component={HomeScreen}
        options={{
          title: 'Home',
          tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} name="md-home" />,
        }}
      />
      <BottomTab.Screen
        name="Favourites"
        component={FavouritesScreen}
        options={{
          title: 'Favourites',
          tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} name="md-heart" />,
        }}
      />
      <BottomTab.Screen
        name="PostAd"
        component={PostAdScreen}
        options={{
          title: 'Post Ad',
          tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} name="md-add" />,
        }}
      />
      <BottomTab.Screen
        name="Messages"
        component={MessagesScreen}
        options={{
          title: 'Messages',
          tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} name="md-mail" badge={13} />,
        }}
      />
      <BottomTab.Screen
        name="Profile"
        component={ProfileScreen}
        options={{
          title: 'Profile',
          tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} name="md-person" />,
        }}
      />
    </BottomTab.Navigator>
  ));


// function getHeaderTitle(route) {
//   const routeName = route.state?.routes[route.state.index]?.name ?? INITIAL_ROUTE_NAME;
//
//   switch (routeName) {
//     case 'Home':
//       return 'Home';
//     case 'Favourites':
//       return 'Favourites';
//     case 'PostAd':
//       return 'Post Ad';
//     case 'Messages':
//       return 'Messages';
//     case 'Profile':
//       return 'Profile';
//   }
// }

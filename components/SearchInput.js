import React from 'react';
import { MaterialIcons } from '@expo/vector-icons';
import { Searchbar } from 'react-native-paper';
import {Platform, StyleSheet} from 'react-native';
import { CONFIG } from '../config';
import axios from "axios";
import {View, Text, FlatList, TouchableOpacity} from "react-native";

export default SearchInput = () => {

  const [loading, setLoading] = React.useState(false);
  const [search, setSearch] = React.useState('');
  const [suggestions, setSuggestions] = React.useState([]);


  const _getSuggestions = (text) => {
    setLoading(true);

    if (text) {
      setSearch(text);
      const query = encodeURIComponent(text);
      axios.get(`${CONFIG.API_URL}/items/?search=${query}`)
        .then((result) => {
          // Process list of suggestions

          setSuggestions(result.data.data);
          console.log({result});

          setLoading(false);
        });
    } else {
      setSuggestions([]);
      setSearch('');
    }

  };

  const _selectSuggestion = (suggestion) => {
    setSearch(suggestion);
    setSuggestions([]);
  };

  const Item = ({ title }) => {
    return (
      <TouchableOpacity style={styles.item} onPress={() => _selectSuggestion(title)}>
        <Text style={styles.title}>{title}</Text>
      </TouchableOpacity>
    );
  }


  return (
    <View>
      <Searchbar
        theme={{ colors: { primary: CONFIG.COLORS.ACCENT, placeholder: CONFIG.BORDER.COLOR } }}
        style={styles.search}
        placeholder="Search for anything..."
        onChangeText={text => _getSuggestions(text)}
        value={search}
        icon={() => <MaterialIcons name="search" size={20} />}
        clearIcon={() => <MaterialIcons name="close" size={20} />}
        onIconPress={()=>_getSuggestions(search)}
      />
      <View style={styles.listContainer}>
        <FlatList
          data={suggestions}
          renderItem={({ item }) => <Item title={item.name} />}
          keyExtractor={(item, index) => `list-item-${index}`}
          style={styles.list}
          onScroll={() => {}}
        />
      </View>
    </View>
  );
};


const styles = StyleSheet.create({
  search: {
    height: 34,
    shadowRadius: 0,
    shadowOffset: {
      width: 0,
      height: 0
    },
  },
  listContainer: {
    position: 'absolute',
    paddingLeft: 45,
    paddingRight: 45,
    ...Platform.select({
      web: {
        top: CONFIG.HEADER.HEIGHT-13,
      },
      ios: {
        marginTop: CONFIG.HEADER.HEIGHT - 35,
      }
    }),
    minWidth: '100%',
  },
  list: {
    borderRadius: CONFIG.BORDER.RADIUS,
    shadowRadius: CONFIG.BUTTON.SHADOW_BLUR,
    shadowOffset: {
      width: 0,
      height: CONFIG.BUTTON.SHADOW_BOTTOM,
    },
    shadowOpacity: CONFIG.BUTTON.SHADOW_OPACITY,
  },
  item: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    backgroundColor: CONFIG.COLORS.WHITE,
    flexGrow: 1,

    ...Platform.select({
      web: {
        cursor: 'pointer',
        '&:hover': {
          backgroundColor: '#ff000e'
        }
      },
      ios: {
        paddingHorizontal: 10,
      }
    }),
  },
  title: {

  }
});

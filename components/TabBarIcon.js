import * as React from 'react';
import {
  Platform, View, StyleSheet, Text
} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { CONFIG } from '../config';

export default function TabBarIcon({ focused, name, badge }) {
  return (
    <View style={focused ? [styles.iconCircle, styles.activeCircle] : styles.iconCircle}>
      <Ionicons
        name={name}
        size={20}
        style={styles.icon}
      />
      {badge ? (
        <View style={styles.badge}>
          <Text style={styles.badgeText}>
            {badge}
          </Text>
        </View>
      ) : null}
    </View>

  );
}

// let mt = Platform.OS === 'ios' || Platform.OS === 'android' ? 0 : 0;

const styles = StyleSheet.create({
  iconCircle: {
    width: 30,
    height: 30,
    // lineHeight: 30,
    borderRadius: 50,
    top: -6,
    left: 0,
    // textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  activeCircle: {
    backgroundColor: CONFIG.COLORS.WHITE,
  },
  icon: {
    // width: '100%',
    marginTop: 0,
    textAlign: 'center',
    color: CONFIG.ICON.COLOR,
    ...Platform.select({
      ios: {
        marginTop: 2,
      },
      android: {
        marginTop: 2,
      },
    }),
  },
  badge: {
    backgroundColor: CONFIG.COLORS.PRIMARY,
    width: 16,
    height: 16,
    // lineHeight: 16,
    top: 0,
    right: -5,
    position: 'absolute',
    borderRadius: 30,
    opacity: 1,
    zIndex: 2,
    padding: 0,
  },
  badgeText: {
    display: 'flex',
    color: '#fff',
    width: 'auto',
    fontSize: 10,
    // lineHeight: 16,
    textAlign: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    marginTop: 0,
  }

});

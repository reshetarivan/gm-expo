import * as React from 'react';
import {
  Platform, StatusBar, StyleSheet, View, Animated
} from 'react-native';
import { SplashScreen } from 'expo';
import * as Font from 'expo-font';
// import { MaterialIcons } from '@expo/vector-icons';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { Provider as StoreProvider } from 'react-redux';
import {
  DefaultTheme, Provider as PaperProvider, Text, TextInput
} from 'react-native-paper';
// import Ionicons from '@expo/vector-icons/build/Ionicons';
import { store } from './redux/store';
// import { PersistGate } from 'redux-persist/integration/react';

import BottomTabNavigator from './navigation/BottomTabNavigator';
import useLinking from './navigation/useLinking';
import Header from './navigation/Header';
import { CONFIG } from './config';
// import HomeScreen from './screens/HomeScreen';
// import TabBarIcon from './components/TabBarIcon';

const Stack = createStackNavigator();

export default function App(props) {
  const [isLoadingComplete, setLoadingComplete] = React.useState(false);
  const [initialNavigationState, setInitialNavigationState] = React.useState();
  const containerRef = React.useRef();
  const { getInitialState } = useLinking(containerRef);

  // const [navbarHeight, setNavbarHeight] = React.useState(0);
  // const _setAnimation = (enable) => {
  //   Animated.timing(navbarHeight, {
  //     duration: 400,
  //     toValue: enable ? CONFIG.FOOTER.HEIGHT : 0
  //   }).start();
  // };


  // Load any resources or data that we need prior to rendering the app
  React.useEffect(() => {
    async function loadResourcesAndDataAsync() {
      try {
        SplashScreen.preventAutoHide();

        // Load our initial navigation state
        setInitialNavigationState(await getInitialState());

        // Load fonts
        await Font.loadAsync({
          // ...Ionicons.font,
          // ...MaterialIcons.font,
          // MaterialCommunityIcons,
          // MaterialDesignIcons,
          'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf'), // eslint-disable-line global-require
          // 'Material-Design-Icons':
          // require('./assets/fonts/MaterialDesignIconsDesktop.ttf'), // eslint-disable-line global-require
          MaterialCommunityIcons: require('./assets/fonts/MaterialCommunityIcons.ttf'), // eslint-disable-line global-require
          MaterialIcons: require('./assets/fonts/MaterialIcons.ttf'), // eslint-disable-line global-require
        });
      } catch (e) {
        // We might want to provide this error information to an error reporting service
        console.warn(e);
      } finally {
        setLoadingComplete(true);
        SplashScreen.hide();
      }
    }

    loadResourcesAndDataAsync();
  }, []);


  if (!isLoadingComplete && !props.skipLoadingScreen) {
    return <Text>...Loading</Text>;
  }


  const styles = StyleSheet.create({
    container: {
      flex: 1,
      margin: 0,
      padding: 0,
      top: 0,
    },
    header: {
      height: CONFIG.HEADER.HEIGHT,
      backgroundColor: CONFIG.COLORS.BACKGROUND,
      shadowColor: CONFIG.BUTTON.SHADOW_BOTTOM_COLOR,
      shadowOffset: {
        width: 0,
        height: CONFIG.BUTTON.SHADOW_BOTTOM
      },
      shadowOpacity: CONFIG.BUTTON.SHADOW_OPACITY,
      shadowRadius: CONFIG.BUTTON.SHADOW_BLUR,
      elevation: CONFIG.BUTTON.ELEVATION,
    },
  });

  StatusBar.setBarStyle('dark-content', true);

  const theme = {
    ...DefaultTheme,
    roundness: 30,
    colors: {
      ...DefaultTheme.colors,
      primary: CONFIG.COLORS.PRIMARY,
      accent: CONFIG.COLORS.ACCENT,
      placeholder: CONFIG.BORDER.COLOR
    },
  };




  return (
    <StoreProvider store={store}>
      <PaperProvider theme={theme}>
        <View style={styles.container}>
          {Platform.OS === 'ios' && <StatusBar barStyle="dark-content" />}
          <NavigationContainer ref={containerRef} initialState={initialNavigationState}>
            <Stack.Navigator
              screenOptions={{
                headerTitle: () => (<Header />),
                headerStyle: styles.header,
                // headerTransparent: true,
                // headerShown: true,
                // headerTitleAlign: 'center',
                // animationEnabled: true,
              }}
            >
              <Stack.Screen
                name="Root"
                component={BottomTabNavigator}
                options={{
                  //   headerTitle: <Header/>,
                  //   headerStyle: styles.header,
                  //   headerShown: true,
                  //   headerTitleAlign: 'center',
                  //   // headerTransparent: false,
                  //   // headerBackground: ()=>(<View style={styles.header} />),
                  //   animationEnabled: true,
                }}
              />
            </Stack.Navigator>
          </NavigationContainer>
        </View>
      </PaperProvider>
    </StoreProvider>
  );
}

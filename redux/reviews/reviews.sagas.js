import {all, call, put, select, takeLatest} from 'redux-saga/effects';
import axios from "axios";
import _ from 'lodash';
import {CONFIG} from "../../config";
import {selectShopReview} from "./reviews.selectors";
import {
  failAddReviewsAction,
  failGetReviewsAction,
  // startGetReviewsAction,
  successAddReviewAction,
  successGetReviewsAction
} from "./reviews.actions";
import {ReviewsTypes} from "./reviews.types";
import {toastr} from "react-redux-toastr";
// import {selectPinsAll} from "../pins/pins.selectors";
import {startGetPinsAction, /*successGetPinsAction,*/ updatePinAction} from "../pins/pins.actions";
// import {fetchPinsAsync} from "../pins/pins.sagas";


export function* fetchReviewsAsync() {
  try {
    const payload = yield select(selectShopReview);
    let reviews =  yield axios.get(`${CONFIG.API_URL}/reviews/${payload.shopId}`)
      .then(res => res.data);
    reviews.rating = 0;
    if (reviews.data.length) {
      reviews.rating = _.floor(_.meanBy(reviews.data, (review) => (parseInt(review.rate))), 1);
    }

    const rates = [
      {rate: 1, val: 0},
      {rate: 2, val: 0},
      {rate: 3, val: 0},
      {rate: 4, val: 0},
      {rate: 5, val: 0},
    ];
    reviews.data.map(review => {
      rates.map(r => {
        if(r.rate === parseInt(review.rate)){
          r.val++;
        }
      })
    });
    reviews.rates = rates;

    yield put(successGetReviewsAction(reviews));

  } catch (error) {
    const errorMessage = error.response?.data ? error.response.data : error.message;
    yield put(failGetReviewsAction(errorMessage));
    toastr.error('Error!', errorMessage);
  }
}


export function* sendReviewAsync(action) {
  try {
    let reviewId = '';
    if (action.payload.id) { reviewId = action.payload.id }
    let review =  yield axios({
      url: `${CONFIG.API_URL}/reviews/${reviewId}`,
      method: 'post',
      data: action.payload,
    })
      .then( rev => rev);

    console.log('review', review);

    if (review?.status === 200) {
      const successMessage = `Your review was successfully ${reviewId ? 'updated' : 'added'}`;
      yield put(successAddReviewAction(successMessage));
      yield toastr.success('Congratulations!', successMessage);
      // yield put(startGetReviewsAction());
      yield call(fetchReviewsAsync);
      yield put(updatePinAction(review.data));
    }

  } catch (error) {
    const errorMessage = error.response?.data ? error.response.data : error.message;
    yield put(failAddReviewsAction(errorMessage));
    toastr.error('Error!', errorMessage);
  }
}


export function* updatePins(action) {
  console.log('updatePins', action.payload.shop);
  yield put(startGetPinsAction());


  // let pins = yield select(selectPinsAll);
  // console.log('pins', pins);
  // // pins.pins.data.map(shop => {
  // //   if (shop.shopId === action.payload.shop.shopId) {
  //     const newPins = pins.pins.data.map(shop => {
  //       if (shop.shopId === action.payload.shop.shopId) {
  //         shop.shopRating = action.payload.shop.shopRating;
  //         shop.shopReviews = action.payload.shop.shopReviews;
  //       }
  //       return shop;
  //     });
  //     pins.pins.data = newPins;
  //
  //     console.log({pins});
  //     yield put(successGetPinsAction(pins.pins));
  //
  // // }
  // // });
}


export function* onStartGetReviews() {
  yield takeLatest(
    ReviewsTypes.START_GET_REVIEWS,
    fetchReviewsAsync
  )
}


export function* onAddReview() {
  yield takeLatest(
    ReviewsTypes.ADD_REVIEW,
    sendReviewAsync
  );
}

export function* onUpdateReview() {
  yield takeLatest(
    ReviewsTypes.UPDATE_REVIEW,
    sendReviewAsync
  );
}

export function* onSetIsOpen() {
  yield takeLatest(
    ReviewsTypes.SET_IS_OPEN,
    updatePins
  );
}


export function* reviewsSagas() {
  yield all([
    call(onStartGetReviews),
    call(onAddReview),
    call(onUpdateReview),
    call(onSetIsOpen),
  ])
}

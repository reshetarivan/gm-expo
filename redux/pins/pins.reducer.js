import {PinsTypes} from "./pins.types";
import {selectPinUtil, updatePinUtil} from "./pins.utils";

const INITIAL_STATE = {
  pins: {
    start: 0,
    totalItems: 0,
    totalShops: 0,
    data: []
  },
  isLoading: false,
  isError: false,
  selectAll: false,
  selected: [],
  isHover: null,
};

const pinsReducer = (state = INITIAL_STATE, action) => {
  let isSelAll = state.selectAll;

  switch (action.type) {

    case PinsTypes.START_GET_PINS:
      return {
        ...state,
        isError: false,
        isLoading: true,
        pins: {
          start: 0,
          totalItems: 0,
          totalShops: 0,
          data: []
        }
      };

    case PinsTypes.SUCCESS_GET_PINS:
      return {
        ...state,
        isError: false,
        isLoading: false,
        pins: action.payload
      };

    case PinsTypes.FAIL_GET_PINS:
      return {
        ...state,
        isError: action.payload,
        isLoading: false,
        pins: {
          start: 0,
          totalItems: 0,
          totalShops: 0,
          data: []
        }
      };

    case PinsTypes.SELECT_PIN:
      // let idxSel = state.pins.indexOf(action.payload);
      //const hasSelected = _.has(state.pins[idxSel], 'selected');
      // console.log({idxSel});
      // console.log({hasSelected});
      // const pin = state.pins[idxSel];
      if (state.selectAll && state.selected.find(pinItem => pinItem.shopId === action.payload.shopId)) {
        isSelAll = false;
      }

      //console.log('state-pin', pin.hasOwnProperty('selected'));

      // if ('selected' in state.pins[idxSel]) {
      //   console.log('HAS SELECTED');
      // }

      // if (hasSelected) {
      //   if (state.pins[idxSel].selected) {
      //     state.pins[idxSel] = {...state.pins[idxSel], selected: true};
      //   } else {
      //     state.pins[idxSel] = {...state.pins[idxSel], selected: false};
      //   }
      // } else {
      //   state.pins[idxSel] = {...state.pins[idxSel], selected: true};
      // }

      return {
        ...state,
        isError: false,
        isLoading: false,
        selectAll: isSelAll,
        selected: selectPinUtil(state.selected, action.payload)
      };

    case PinsTypes.SELECT_ALL_PINS:
      return {
        ...state,
        isError: false,
        isLoading: false,
        selectAll: true,
        selected: state.pins.data
      };

    case PinsTypes.DESELECT_ALL_PINS:
      return {
        ...state,
        isError: false,
        isLoading: false,
        selectAll: false,
        selected: []
      };

    case PinsTypes.CLEAR_PINS:
      return {
        ...state,
        isError: false,
        isLoading: false,
        selectAll: false,
        pins: {
          start: 0,
          totalItems: 0,
          totalShops: 0,
          data: []
        }
      };

    case PinsTypes.SET_IS_HOVER:
      return {
        ...state,
        isHover: action.payload,
      };

    case PinsTypes.UPDATE_PIN:
      console.log('UPDATE_PIN', action);
      return {
        ...state,
        pins: {
          ...state.pins,
          data: updatePinUtil(state.pins.data, action.payload)
        }
      };

    default:
      return state;
  }
};

export default pinsReducer;

import {PinsTypes} from "./pins.types";

export const startGetPinsAction = (query) => ({
  type: PinsTypes.START_GET_PINS,
  payload: query
});

export const successGetPinsAction = pins => ({
  type: PinsTypes.SUCCESS_GET_PINS,
  payload: pins
});

export const failGetPinsAction = error => ({
  type: PinsTypes.FAIL_GET_PINS,
  payload: error
});

export const selectPinAction = pin => ({
  type: PinsTypes.SELECT_PIN,
  payload: pin
});

export const selectAllPinsAction = () => ({
  type: PinsTypes.SELECT_ALL_PINS
});

export const deselectAllPinsAction = () => ({
  type: PinsTypes.DESELECT_ALL_PINS
});

export const clearPinsAction = () => ({
  type: PinsTypes.CLEAR_PINS
});

export const setIsHoverAction = (shop) => ({
  type: PinsTypes.SET_IS_HOVER,
  payload: shop
});

export const updatePinAction = (shop) => ({
  type: PinsTypes.UPDATE_PIN,
  payload: shop
});

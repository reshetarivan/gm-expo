import {GeoTypes} from "./geo.types";

const INITIAL_STATE = {
  lat: 0,
  lng: 0,
  ne_lat: 0,
  ne_lng: 0,
  sw_lat: 0,
  sw_lng: 0,
  timezone: '',
  isError: false,
  isLoading: false,
};

const geoReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {

    case GeoTypes.START_GET_GEO:
      return {
        ...state,
        isError: false,
        isLoading: true,
      };

    case GeoTypes.SUCCESS_GET_GEO:
      return {
        ...state,
        lat: action.payload.lat,
        lng: action.payload.lng,
        isError: false,
        isLoading: false,
      };

    case GeoTypes.FAIL_GET_GEO:
      return {
        ...state,
        isError: true,
        isLoading: false,
      };

    case GeoTypes.CHANGE_BOUNDS:
      return {
        ...state,
        ne_lat: action.payload.ne_lat,
        ne_lng: action.payload.ne_lng,
        sw_lat: action.payload.sw_lat,
        sw_lng: action.payload.sw_lng,
        isError: false,
        isLoading: false,
      };

    default:
      return {...state}
  }
};

export default geoReducer;

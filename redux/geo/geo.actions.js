import {GeoTypes} from "./geo.types";

export const startGetGeoAction = (query) => ({
  type: GeoTypes.START_GET_GEO,
  payload: query
});

export const successGetGeoAction = (query) => ({
  type: GeoTypes.SUCCESS_GET_GEO,
  payload: query
});

export const failGetGeoAction = (query) => ({
  type: GeoTypes.FAIL_GET_GEO,
  payload: query
});

export const changeBoundsAction = (query) => ({
  type: GeoTypes.CHANGE_BOUNDS,
  payload: query
});

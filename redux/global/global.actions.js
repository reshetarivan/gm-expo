export const showNavAction = option => ({
  type: 'SHOW_NAV',
  payload: option
});

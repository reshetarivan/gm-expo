import {createSelector} from "reselect";

const selectGlobal = state => state.global;

export const selectShowNav = createSelector(
  [selectGlobal],
  selectGlobal => selectGlobal.showNav
);

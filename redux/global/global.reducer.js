const INITIAL_STATE = {
  showNav: true,
};

const globalReducer = (state = INITIAL_STATE, action) => {

  switch (action.type) {
    case 'SHOW_NAV':
      return {
        ...state,
        showNav: action.payload
      };
    default:
      return state;
  }
};

export default globalReducer;

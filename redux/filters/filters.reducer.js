import {FiltersTypes} from "./filters.types";

const INITIAL_STATE = {
  options: {
    categories: {
      data: [],
      isError: false,
      isLoading: false,
    }
  },
  type: '',
  search: '',
  searchImg: '',
  category: '',
  minPrice: 0,
  maxPrice: 0,
  fromPrice: 0,
  toPrice: 0,
  suggestions: [],
  isLoading: false,
  isError: false
};

const filtersReducer = (state = INITIAL_STATE, action) => {

  let {search, type, category, searchImg} = action.payload ? action.payload : INITIAL_STATE;

  switch (action.type) {

    case FiltersTypes.INPUT_SEARCH:
      return {
        ...state,
        search: action.payload
      };

    case FiltersTypes.START_GET_SUGGESTION:
      return {
        ...state,
        //suggestions: [],
        isError: false,
        isLoading: true,
      };

    case FiltersTypes.SUCCESS_GET_SUGGESTIONS:
      return {
        ...state,
        suggestions: action.payload,
        isError: false,
        isLoading: false,
      };

    case FiltersTypes.FAIL_GET_SUGGESTION:
      return {
        ...state,
        suggestions: [],
        isError: action.payload,
        isLoading: false,
      };

    case FiltersTypes.SELECT_SUGGESTION:
      return {
        ...state,
        search,
        searchImg,
        type,
        category,
        isError: false,
        isLoading: false,
      };

    case FiltersTypes.SET_TYPE:
      return {
        ...state,
        type: action.payload,
      };

    case FiltersTypes.CLEAR_SEARCH:
      return INITIAL_STATE;

    case FiltersTypes.START_FETCH_CATEGORIES:
      return {
        ...state,
        options: {
          ...state.options,
          categories: {
            data: [],
            isError: false,
            isLoading: true,
          }
        },
      };

    case FiltersTypes.SUCCESS_FETCH_CATEGORIES:
      return {
        ...state,
        options: {
          ...state.options,
          categories: {
            data: action.payload,
            isError: false,
            isLoading: false,
          }
        },
      };

    case FiltersTypes.FAIL_FETCH_CATEGORIES:
      return {
        ...state,
        options: {
          ...state.options,
          categories: {
            data: [],
            isError: action.payload,
            isLoading: false,
          }
        },
      };

    case FiltersTypes.SET_CATEGORY:
      return {
        ...state,
        category: action.payload,
      };

    case FiltersTypes.SET_PRICE_RANGE:
      return {
        ...state,
        minPrice: action.payload.minPrice,
        maxPrice: action.payload.maxPrice,
        fromPrice: action.payload.fromPrice,
        toPrice: action.payload.toPrice,
      };

    case FiltersTypes.UPDATE_PRICE_RANGE:
      return {
        ...state,
        fromPrice: action.payload.fromPrice,
        toPrice: action.payload.toPrice,
      };

    case FiltersTypes.CLEAR_FILTERS:
      return {
        ...state,
        type: '',
        category: '',
        fromPrice: 0,
        toPrice: 0,
      };

    default:
      return state;
  }
};

export default filtersReducer;

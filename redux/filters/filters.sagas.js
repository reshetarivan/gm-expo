import {takeLatest, put, all, call, debounce, select} from 'redux-saga/effects';
import {FiltersTypes} from "./filters.types";
import {
  failFetchCategoriesAction,
  failGetSuggestionAction,
  startGetSuggestionAction,
  successFetchCategoriesAction,
  successGetSuggestionAction
} from "./filters.actions";
import axios from "axios";
import rateLimit from 'axios-rate-limit';
import {CONFIG} from "../../config";
import {clearPinsAction, deselectAllPinsAction} from "../pins/pins.actions";
import {selectFiltersAll} from "./filters.selectors";
import {startGetItemsAction} from "../items/items.actions";

const api = rateLimit(axios.create(), { maxRequests: 1, perMilliseconds: CONFIG.REQUEST_DELAY });

/* SUGGESTIONS */

export function* fetchSuggestionsAsync({payload}) {
  try {
    yield put(startGetSuggestionAction());
    let suggestionData =  yield api.get(`${CONFIG.API_URL}/items/${payload}`)
      .then(res => res.data);
    yield put(successGetSuggestionAction(suggestionData));
  } catch (error) {
    yield put(failGetSuggestionAction(error.message))
  }
}

export function* onSelectSuggestion() {
  yield takeLatest(
    FiltersTypes.SELECT_SUGGESTION,
    getItems
  )
}

/* PINS */

export function* clearPins() {
  yield put(clearPinsAction());
  yield put(deselectAllPinsAction());
}

export function* getItems() {
  const payload = yield select(selectFiltersAll);
  yield put(startGetItemsAction(payload));
}


/* SEARCH */

export function* onStartInputSearch() {
  yield debounce(CONFIG.REQUEST_DELAY,
    FiltersTypes.INPUT_SEARCH,
    fetchSuggestionsAsync
  )
}

export function* onClearSearch() {
  yield takeLatest(
    FiltersTypes.CLEAR_SEARCH,
    clearPins
  )
}


/* CATEGORIES */

export function* fetchCategoriesAsync({payload}) {
  try {
    const categoriesData =  yield api.get(`${CONFIG.API_URL}/categories/${payload}`)
      .then(res => res.data);
    yield put(successFetchCategoriesAction(categoriesData));
  } catch (error) {
    yield put(failFetchCategoriesAction(error.message));
  }
}

export function* fetchCategoriesStart() {
  yield takeLatest(
    FiltersTypes.START_FETCH_CATEGORIES,
    fetchCategoriesAsync
  )
}

export function* onChangeFilter() {
  yield debounce(CONFIG.REQUEST_DELAY,
    [
      FiltersTypes.SET_TYPE,
      FiltersTypes.SET_CATEGORY,
      FiltersTypes.UPDATE_PRICE_RANGE,
      FiltersTypes.CLEAR_FILTERS,
    ],
    getItems
  )
}



export function* filtersSagas() {
  yield all([
    call(onStartInputSearch),
    call(onClearSearch),
    call(onSelectSuggestion),
    call(fetchCategoriesStart),
    call(onChangeFilter),
  ])
}

import { all, call } from 'redux-saga/effects';
import { filtersSagas } from './filters/filters.sagas';
import { pinsSagas } from './pins/pins.sagas';
import { reviewsSagas } from './reviews/reviews.sagas';
import { itemsSagas } from './items/items.sagas';

export default function* rootSaga() {
  yield all([
    call(filtersSagas),
    call(pinsSagas),
    call(reviewsSagas),
    call(itemsSagas),
  ]);
}

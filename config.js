import { Dimensions, Platform } from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';

const { width } = Dimensions.get('window');
const { height } = Dimensions.get('window');


export const CONFIG = {

  API_URL: window?._env_?.API_URL ? window._env_.API_URL : 'http://localhost:4000',
  REQUEST_DELAY: 500, // milliseconds

  WINDOW: {
    WIDTH: width,
    HEIGHT: height,
  },

  SCREEN: {
    XS: width < 576, // Extra small smartphones
    SM: width > 576, // Small tablets and large smartphones (landscape view)
    MD: width > 768, // Small tablets (portrait view)
    LG: width > 992, // Tablets and small desktops
    XL: width > 1200, // Large tablets and desktops
  },


  COLORS: {
    BACKGROUND: '#E0E5EC',
    PRIMARY: '#e91e63', // pink
    ACCENT: '#13ABCC',
    CONTENT: '#FFFFFF',
    DEFAULT: '#9e9e9e',
    SECONDARY: '#464b52', // blueGray
    INFO: '#2196f3',
    ERROR: '#F44336',
    WHITE: '#FFFFFF',
    BLACK: '#333333',
    BLUE: '#214BEE',
    DARK_NAVY: '#04264C',
    CYAN: '#00bcd4',
    PURPLE: '#AA00FF',
    PURPLE_LIGHT: '#A9A7EB',
    GRAY_WHITE: '#f1f1f1',
    GRAY_WHITEN: '#f6f6f6',
    GRAY_LIGHT: '#e0e0e0',
    GRAY_HOVER: '#d5dae1',
    GRAY_CLOUD: '#8D99AE',
    GRAY_BLUE: '#607d8b',
    GRAY_DARK: '#54575E',
    TURQOUISE: '#2D728F',
    TEAL: '#009688',
    BLUE_LIGHT: '#03a9f4', // #38B0DE
    BLUE_PASTEL: '#006494',
    GREEN: '#4caf50',
    PINK: '#e91e63',
    YELLOW_LIGHT: '#FFF2CC',
  },

  SHADOW: {
    BASE: '0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23)',
    LIGHT: '0px 0px 11px rgba(45, 45, 45, 0.11)',
    LIGHT_INSET: 'inset 0px 0px 11px rgba(45, 45, 45, 0.11)',
    PURPLE: 'linear-gradient(90deg, rgba(78,113,251,1) 0%, rgba(158,155,255,1) 100%)',
  },

  HEADER: {
    HEIGHT: 50 + (getStatusBarHeight() || 0),
  },

  DRAWER: {
    WIDTH: 400,
    HEIGHT: 50,
  },

  FOOTER: {
    HEIGHT: 60
  },

  SIDEBAR: {
    WIDTH: 200,
    WIDTH_MIN: 150,
    MARGIN_LEFT: -200,
    TRANSITION_SPEED: '400ms',
    TITLE_HEIGHT: 50,
  },

  CART: {
    BG_COLOR: window.COLORS?.WHITE,
    BG_TOTAL: window.COLORS?.GRAY_WHITE,
  },

  BORDER: {
    COLOR: '#CCCCCC',
    RADIUS: 4
  },

  SPACE: {
    BASE: 8,
  },

  ICON: {
    WIDTH: 50,
    HEIGHT: 50,
    COLOR: window.COLORS?.BLACK,
  },

  BUTTON: {
    TEXT_COLOR_INVERT: window.COLORS?.WHITE,
    TEXT_COLOR_PRIMARY: window.COLORS?.PRIMARY,
    ICON_COLOR: window.COLORS?.BLACK,
    ICON_MARGIN: window.SPACE?.BASE,
    COLOR_DEFAULT: window.COLORS?.PRIMARY,
    COLOR_HOVER: window.COLORS?.GRAY_HOVER,
    SHADOW_TOP_COLOR: 'rgba(255,255,255, 1)',
    SHADOW_BOTTOM_COLOR: 'rgba(163,177,198,0.6)',
    SHADOW_TOP: -2,
    SHADOW_RIGHT: 2,
    SHADOW_BOTTOM: 2,
    SHADOW_LEFT: -2,
    SHADOW_BLUR: 4,
    SHADOW_OPACITY: 1,
    ELEVATION: 8,
  },

  TEXT: {
    COLOR_DEFAULT: window.COLORS?.BLACK,
    COLOR_SECONDARY: window.COLORS?.SECONDARY,
  },

  LINK: {
    COLOR: window.COLORS?.GRAY_BLUE,
  },


  ERROR: {
    TEXT_COLOR: window.COLORS?.ERROR,
  },


};
